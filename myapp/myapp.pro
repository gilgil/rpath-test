TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
DESTDIR = ${PWD}/../bin
LIBS += -L${PWD}/../bin -lmylib # tell linker "-Wl,-rpath,\$ORIGIN -Wl,-rpath,\$ORIGIN/../lib"
QMAKE_RPATHDIR += . ../lib
SOURCES += main.cpp
