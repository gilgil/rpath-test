rpath
===

executable file searches so(shared object file) in system folder. If you give rpath option in linking, executable file searches so file in a relative folder.

in Qt pro file, you insert the following option.

```
QMAKE_RPATHDIR += . ../lib
```

And then, g++ linker add the following link option.

```
-Wl,-rpath,\$ORIGIN -Wl,-rpath,\$ORIGIN/../lib
```

When executable file launches, it searches so file in the same folder or "../lib" folder.
